import React, {Component} from 'react';
import Input from './todo_input';

export default class TodoItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            newValue:this.props.todo.text,
        }
        this.removeTodo = this.removeTodo.bind(this);
        this.editTodo = this.editTodo.bind(this);
        this.showTodo = this.showTodo.bind(this);
    }

    removeTodo(id) {
        this.props.removeTodo(id);
    }

    editTodo() {
        this.setState({
            edit: true,
        });
    }
    showTodo(todo) {
        this.setState({
            newValue:todo,
            edit: false,
        });
    }

    render() {
        return (
            <div>
                <span> {this.state.newValue} </span>
                {
                    this.state.edit ? <Input editValue={this.props.todo.id} title="Edit" es="True" edit={(e)=>this.props.edit(e)} s={(e)=>this.showTodo(e)}/>
                        : <div>
                            <button className="btn btn-primary" type="button"
                                    onClick={() => this.removeTodo(this.props.id)}>
                                Remove
                            </button>
                            <button className="btn btn-sm btn-primary" onClick={() => this.editTodo()}> Edit
                            </button>
                        </div>
                }
            </div>
        );

    }
}
