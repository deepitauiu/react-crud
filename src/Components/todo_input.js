import React, {Component} from 'react';


export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            es:this.props.es
        };
        this.handleChange = this.handleChange.bind(this);
        this.addtodo = this.addtodo.bind(this);
        this.editTodo = this.editTodo.bind(this);
    }

    handleChange(e) {
        this.setState({
            value: e.target.value
        });
        // console.log("list", this.state.value);
    }

    addtodo(todo) {
        if (todo.length > 0) {
            this.props.addTodo(todo);
            this.setState({
                value: ""
            });
        }
    }

    editTodo(todo) {
        if (todo.length > 0) {
            let x= { id : this.props.editValue, text: todo};
            this.props.edit(x);
            this.props.s(todo);
            this.setState({
                value: "",
                es: "x"
            });
        }
    }

    render() {
        return (
            <div>
                {
                    this.state.es === "False" ?
                        <span>
                        <input type="text" name="list" value={this.state.value} onChange={(e) => this.handleChange(e)}/>
                        <button className="btn btn-sm btn-primary"
                                onClick={() => this.addtodo(this.state.value)}>{this.props.title}</button>
                        </span>
                        : this.state.es === "True" ? <span>
                        <input type="text" name="list" value={this.state.value} onChange={(e) => this.handleChange(e)}/>
                        <button className="btn btn-sm btn-primary"
                                onClick={() => this.editTodo(this.state.value)}>{this.props.title}</button>
                        </span>: ""
                }
            </div>
        );

    }
}
