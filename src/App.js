import React, {Component} from 'react';
import './App.css';
import List from './Components/list';
import Input from './Components/todo_input';
import TodoItem from './Components/todo_item';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todo: [
                {id: 0, text: "1st todo"},
                {id: 1, text: "2nd todo"},
                {id: 2, text: "3rd todo"},
            ],
            next_id: 3
        };
        this.addTodo = this.addTodo.bind(this);
        this.removeTodo = this.removeTodo.bind(this);
        this.edit = this.edit.bind(this);
    }

    addTodo(todo) {
        // console.log("addtodo ", todo);
        let todos = this.state.todo.slice();
        todos.push({id: this.state.next_id, text: todo});
        let next_id = this.state.next_id + 1;
        this.setState({
            next_id: next_id,
            todo: todos,
        });
    }
    edit(todo) {
       if(todo.length>0){
           console.log("edit ", todo.id);
           let todos = this.state.todo.slice();
           todos=todos.map((t,index)=> {
               return(

                   t.id === todo.id ? t=todo : t=t
               )
           });
           this.setState({
               todo: todos,
           });
       }
    }

    removeTodo(id) {
        // console.log("remove ", id);
        let todos = this.state.todo.filter((todo, index) => todo.id !== id);
        this.setState({
            todo: todos,
        });

    }

    render() {
        const todos = this.state.todo.map((todo, index) => {
            return (
                <TodoItem todo={todo}
                          key={todo.id}
                          id={todo.id}
                          removeTodo={(e) => this.removeTodo(e)}
                          edit={(e)=>this.edit(e)}
                />)
        });
        return (
            <div className="App">
                <header>To do app</header>
                <div className="todo-wrapper">
                    <List/>
                    <Input addTodo={(e) => this.addTodo(e)} title="Add" es="False"/>
                    <ul>
                        {todos}
                    </ul>
                </div>
            </div>
        );
    }
}

export default App;
